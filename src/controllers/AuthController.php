<?php namespace Sixoh\RedditOAuth;

class AuthController extends \BaseController {

    public function loginAction()
    {
        return RedditOAuth::authRequest();
    }

    public function oauthCallbackAction()
    {
        $code = \Input::get('code');
        $token = \RedditOAuth::getToken($code);
        $token = json_decode($token->body, true);

        $headers = [
            "Authorization" => "bearer ".$token['access_token']
        ];

        $url = 'https://oauth.reddit.com/api/v1/me.json';

        $userInfo = json_decode(\Requests::get($url, $headers)->body);
        foreach($userInfo as $key => $value)
            echo $key.": ".$value."<br />";
    }

}
