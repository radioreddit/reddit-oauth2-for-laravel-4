<?php

namespace Sixoh\RedditOAuth;

use Illuminate\Support\ServiceProvider;
use Sixoh\RedditOAuth\Service\RedditOAuth;
use \Config;

class RedditOAuthServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('sixoh/reddit-oauth');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
    {
        // Register Reddit Oauth
        $this->app->bind('oauth', function() {
            return new RedditOAuth();
        });
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
