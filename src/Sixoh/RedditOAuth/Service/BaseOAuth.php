<?php namespace Sixoh\RedditOAuth\Service;

use Sixoh\RedditOAuth\Service\Interfaces\OAuthInterface;
use \Requests;

abstract class BaseOAuth implements OAuthInterface {

    protected   $clientId,
                $secret,
                $state,
                $baseUrl,
                $authUrl,
                $scope,
                $validScopes;

    public function getClientId() { return $this->clientId; }
    public function getSecret() { return $this->secret; }
    public function getState() { return $this->state; }
    public function getBaseUrl() { return $this->baseUrl; }
    public function getAuthUrl() { return $this->authUrl; }
    public function getScope() { return $this->scope; }

    public function setClientId($client_id) { $this->clientId = $client_id; }
    public function setSecret($secret) { $this->secret = $secret; }
    public function setState($state) { $this->state = $state; }
    public function setBaseUrl($baseUrl) { $this->baseUrl = $baseUrl; }
    public function setAuthUrl($authUrl) { $this->authUrl = $authUrl; }
    public function setScope($scope) { $this->scope = $scope; }

    public function __construct()
    {
        $this->clientId = \Config::get('reddit-oauth::reddit_id');
        $this->secret = \Config::get('reddit-oauth::reddit_secret');
        $this->redirectUri = \Config::get('reddit-oauth::redirect_uri');
        $this->scope = \Config::get('reddit-oauth::reddit_scope');
        $this->responseType = 'code';

        if(!\Session::has('oauthState')) {
            $this->state = $this->generateRandomState();
            \Session::put('oauthState', $this->state);
        }
    }

    public function authRequest()
    {
        if(!$this->validateScopes()) return false;

        return \Redirect::to($this->generateAuthUrl());
    }

    public function getToken($code)
    {
        $url = $this->getTokenUrl();

        $headers = [
            'Authorization' => 'Basic '.base64_encode($this->clientId.":".$this->secret)
        ];

        $params = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $this->redirectUri
        ];

        return \Requests::post($url, $headers, $params);

    }

    public function generateRandomState()
    {
        $state = uniqid('', true);
        return $state;
    }

    public function confirmState($state)
    {
        if(\Session::has('oauthState')) {
            return (\Session::get('oauthState') == $state);
        }
    }

    public function validateScopes()
    {
        $pass = true;

        // Convert Scope from String if its a string
        if(is_string($this->scope)) {
            $scopes = explode(',', $this->scope);
        } else {
            $scopes = $this->scope;
        }

        foreach($scopes as $scope) {
            if(!in_array($scope, $this->validScopes)) {
                $pass = false;
            };
        }

        return $pass;
    }

}
