<?php

namespace Sixoh\RedditOAuth\Service\Interfaces;

interface OAuthInterface {

    function setState($state);
    function generateRandomState();
    function auth();
    function authCallback();
    function getToken($code);
    function validateScopes();
    function getAuthUrl();
    function getTokenUrl();

}
