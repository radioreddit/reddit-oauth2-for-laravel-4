<?php namespace Sixoh\RedditOAuth\Service;

/**
 * Reddit OAuth2 Implementation
 * Author: Cody Halovich @codyhalovich
 * Date: May 1, 2014
 */
class RedditOAuth extends BaseOAuth {

    protected   $baseUrl = 'https://ssl.reddit.com/api/v1/',
                $duration = 'permanent',
                $validScopes = [
                    'edit',
                    'history',
                    'identity',
                    'mysubreddits',
                    'privatemessages',
                    'read',
                    'save',
                    'submit',
                    'subscribe',
                    'vote',
                    'modconfig',
                    'modflair',
                    'modlog',
                    'modpost'
                    ];

    /**
     * Generate the URL for login
     *
     * @return String
     */
    public function generateAuthUrl()
    {
        $state = $this->generateRandomState();
        return $this->baseUrl . "authorize?client_id={$this->clientId}&response_type=code&state={$state}&redirect_uri={$this->redirectUri}&duration={$this->duration}&scope={$this->scope}";
    }

    public function authRequest()
    {
        if(!$this->validateScopes()) return false;

        return \Redirect::to($this->generateAuthUrl());
    }

    /**
     * Generate URL for requesting Token
     *
     * @return String
     */
    public function getTokenUrl()
    {
        return $this->baseUrl . "access_token";
    }

    /**
     * Authorize User
     *
     * @return void
     */
    public function auth()
    {
        parent::auth();
    }

    /**
     * Handle Callback from OAuth provider
     *
     * @return void
     */
    public function authCallback()
    {
        parent::authCallback;
    }


}
