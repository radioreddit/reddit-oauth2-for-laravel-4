<?php

namespace Sixoh\RedditOAuth;

use Illuminate\Support\Facades\Facade;

class RedditOAuth extends Facade {
    protected static function getFacadeAccessor() { return 'oauth'; }
}

?>
