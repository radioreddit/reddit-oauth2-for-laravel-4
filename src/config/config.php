<?php

return array(
    'reddit_id' => $_ENV['reddit']['id'],
    'reddit_secret' => $_ENV['reddit']['secret'],
    'reddit_scope' => $_ENV['reddit']['scope'],
    'redirect_uri' => $_ENV['reddit']['redirect_uri']
);
